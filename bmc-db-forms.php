<?php
/**
 * Plugin Name: BMC Form DB
 * Plugin URI: https://i-tul.com/
 * Description: Custom Form DB for BMC
 * Version: 1.0.0
 * Author: Rick Applegarth
 * Author URI: https://i-tul.com/
 * License: GPL2
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('BMC_Form_DB')) :

class BMC_Form_DB {

    public function __construct() {
        add_action('admin_menu', array($this, 'create_bmcdb_admin_page'));
    }

    public function create_bmcdb_admin_page() {
        add_menu_page(
            'BMC Forms DB', 
            'BMC Forms DB', 
            'manage_options', 
            'bmc-forms-db', 
            array($this, 'display_bmcdb_admin_page_content')
        );
    }

    public function display_bmcdb_admin_page_content() {
        if (!is_plugin_active('contact-form-cfdb7/contact-form-cfdb-7.php')) {
            echo '<p>This plugin only works if Contact Form CFDB7 is installed and active.</p>';
            return;
        }
        global $wpdb;

        $num_per_page = 50; // Define how many records per page you want
        $table_name = $wpdb->prefix . 'db7_forms';
        $page_number = isset($_GET['pg']) ? absint($_GET['pg']) : 1;
        $offset = ($page_number - 1) * $num_per_page;

        $total = $wpdb->get_var("SELECT COUNT(*) FROM $table_name");
        $num_pages = ceil($total / $num_per_page);


        // Execute the query
        $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name ORDER BY form_id DESC LIMIT %d OFFSET %d", $num_per_page, $offset));

        echo '
            <style>
                .bmc_table {
                    margin-bottom:20px;
                    width: calc(100% - 50px);
                }
                .bmc_table .lbl {
                    font-weight: 800;
                    padding: 5px 10px;
                    background-color: #034988;
                    color: white;
                    white-space: nowrap;
                }
                .bmc_table td {
                    vertical-align: top;
                }
                .bmc_table tr {
                    background-color: #ffffff;
                }
                .bmc_table .content {
                    padding: 10px;
                }
        }
            </style>
        ';
        echo "<h1>BMC Form DB</h1>";


        foreach($results as $row) {
            echo '<table class="bmc_table" cellspacing="0" cellpadding="0">';
                $id = $row->form_post_id; 
                $date = $row->form_date; 
                $data = unserialize($row->form_value); 

                echo '<tr>';
                  echo '<td><div class="lbl">Date:</div><div class="content">'. $date .'</div></td>'; 
                  echo '<td><div class="lbl">Form ID:</div><div class="content"><a href="https://communities.ituldev.com/wp-admin/admin.php?page=wpcf7&post=' . $id . '&action=edit">'. $id .'</a></div></td>'; 

                foreach($data as $key => $value) {
                    if(is_array($value)) {
                        $value = implode(", ", $value); // If the value is an array, turn it into a comma-separated string
                    }
                    $key = htmlspecialchars($key);
                    if( $key == "fname" ){ $key = "First"; };
                    if( $key == "lname" ){ $key = "Last"; };
                    if( $key == "your-recipients" ){ $key = "Interest"; };
                    if( $key == "your-recipients2" ){ $key = "Interest 2"; };
                    if( $key == "p-name" ){ $key = "Name"; };
                    if( $key == "p-phone" ){ $key = "Phone"; };
                    if( $key == "p-email" ){ $key = "Email"; };

                    // Display the label in bold on its own line, and the value on the next line
                    if( $key != "cfdb7_status" ):
                    if( $key != "your-recipientsthisisaplaceholder" ):
                        echo '<td><div class="lbl">' . $key . ':</div><div class="content">' . htmlspecialchars($value) . '</div></td>'; 
                    endif;
                    endif;
                }
                
                echo '</tr>';
            echo '</table>';
        }
        echo '<div style="text-align: center;">';
        if ($page_number > 1) {
            echo '<a href="' . add_query_arg('pg', $page_number - 1) . '">< Previous</a> ';
        }

        if ($page_number < $num_pages) {
            echo '<a href="' . add_query_arg('pg', $page_number + 1) . '">Next ></a>';
        }
        echo '</div>';

    }
}

endif;

/**
 * Main instance of BMC_Form_DB.
 *
 * Returns the main instance of BMC_Form_DB to prevent the need to use globals.
 *
 * @return BMC_Form_DB
 */
function BMC_Form_DB() {
    return new BMC_Form_DB();
}

$GLOBALS['bmc_form_db_plugin'] = BMC_Form_DB();
